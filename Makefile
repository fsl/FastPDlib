include ${FSLCONFDIR}/default.mk

PROJNAME = FastPD
SOFILES  = libfsl-fastPD.so

all: ${SOFILES}

libfsl-fastPD.so: FastPD.o graph.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}
